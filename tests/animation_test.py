from solid2 import cube, get_animation_time

cube(5, True).rotateZ(get_animation_time() * 360).save_as_scad()
