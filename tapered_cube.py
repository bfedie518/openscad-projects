from solid2 import polyhedron


def tapered_cube(x_bottom, y_bottom, x_top, y_top, z):
    bottom = [(0, 0, 0), (x_bottom, 0, 0), (x_bottom, y_bottom, 0), (0, y_bottom, 0)]
    x_offset = (x_bottom - x_top) / 2
    y_offset = (y_bottom - y_top) / 2
    top = [
        (x_offset, y_offset, z),
        (x_top + x_offset, y_offset, z),
        (x_top + x_offset, y_top + y_offset, z),
        (x_offset, y_top + y_offset, z),
    ]
    points = bottom + top
    faces = [
        (0, 1, 2, 3),
        (4, 5, 6, 7),
        (0, 1, 5, 4),
        (1, 2, 6, 5),
        (2, 3, 7, 6),
        (0, 3, 7, 4),
    ]
    return polyhedron(points, faces)


if __name__ == "__main__":
    tapered_cube(4, 5, 2, 3, 5).save_as_scad()
